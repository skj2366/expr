var exp = require('express');
var app = exp();

app.get('*',(req,res,next)=>{
    if(req.path.indexOf('test')!=-1){
        res.sendFile(__dirname+'/test.html');
    }else if(req.path.indexOf('info')!=-1){
        res.sendFile(__dirname+'/info.html');
    }else if(req.path.indexOf('tablesort')!= -1){
        res.sendFile(__dirname+'/tablesort.html');
    }else if(req.path.indexOf('jquery')!=-1){
        res.sendFile(__dirname+'/jqueryTable.html');
    }else if(req.path.indexOf('index2')!=-1){
        res.sendFile(__dirname+'/index2.html');
    }else if(req.path.indexOf('modal')!=-1){
        res.sendFile(__dirname+'/modal.html');
    }else{
        res.sendFile(__dirname+'/index.html');
    }
})

app.post('*',(req,res,next)=>{
    res.send('hello post!');
})

app.listen(80,function(){
    console.log(this);
})

// app.listen(80,()=>{
//     console.log(this);
// })
